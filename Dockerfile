FROM ubuntu
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y jp2a
WORKDIR /tmp/
ADD https://o.aolcdn.com/images/dims3/GLOB/legacy_thumbnail/630x315/format/jpg/quality/85/http%3A%2F%2Fi.huffpost.com%2Fgen%2F4529752%2Fimages%2Fn-POKEMON-628x314.jpg img.jpg
ENV TERM xterm-256color
CMD jp2a --size=150x40 img.jpg


